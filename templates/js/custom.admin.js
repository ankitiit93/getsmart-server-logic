var app = angular.module('smart-display-admin', ["isteven-multi-select"]);


// initialize time select widget
// ptTimeSelect doesn't trigger change event by default
$('#validity .time').ptTimeSelect({
    'onClose': function($self) {
        $self.trigger('change');
    }
});
// Initialize date picking widget
$('#validity .date').pikaday();

// Time format displayed
var TIMEFORMAT = 'h:mm a';
// Initialize datepair library for selecting validity from-till dates
var datepair = new Datepair(document.getElementById('validity'), {
    parseTime: function(input){
        // use moment.js to parse time
        var m = moment(input.value, TIMEFORMAT);
        return m.toDate();
    },
    updateTime: function(input, dateObj){
        // use moment.js to parse time
        var m = moment(dateObj);
        return m.format(TIMEFORMAT);
    },
    parseDate: function(input){
        var picker = $(input).data('pikaday');
        return picker.getDate();
    },
    updateDate: function(input, dateObj){
        var picker = $(input).data('pikaday');
        return picker.setDate(dateObj);
    }
});

// Approve content request
function approve(elem, approve) {
    var contetnId = $(elem).parent().parent().attr('data-notice');
    // Get csrf token value
    var csrftoken = getCookie('csrftoken');
    // Update the approval status
    $.ajax({
        url: '/approveContent/',
        type: 'post',
        headers: {
            'Content-Type': undefined,
            'X-CSRFToken': csrftoken
        },
        // If approve = 1 then approve else un-approve
        data: {
            'content_id': contetnId,
            'approve': approve
        },
        dataType: 'json',
        success: function (response) {
            console.log(response);
            // Check for error codes
            if(response.status == 0) {
                alert(response.info);
                return
            }
            alert(response.data);
            // Update the approval status by reloading the page
            location.reload();
        },
        error: function(xhr, textStatus, errorThrown){
            alert('OOPS!!! Something went wrong.');
        }

    });
    return; 
}

// Get cookie from key name
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

// Main app controller
app.controller('MainController', ['$scope', '$http', function($scope, $http) {

    $scope.allChannels = allChannels;

    var vm = this;
    // Initial filled values in the notice. Just for dev purposes
    VIRGIN_NOTICE = {
        content_type: 0,
        title: 'hahaha woooh',
        description: 'Mux is awesome',
        link: 'http://leleo.com',
        channel_ids: [],
        valid_from: new Date(2010, 11, 28, 14, 57),
        valid_till: new Date(2010, 11, 28, 14, 57),
    };
    // Initial filled values in the notice.
    VIRGIN_NOTICE_2 = {
        content_type: 0,
        title: '',
        description: '',
        link: '',
        channel_ids: [],
        valid_from: new Date(2010, 11, 28, 14, 57),
        valid_till: new Date(2010, 11, 28, 14, 57),
    };
    // Default upload error to show in case something goes wrong 
    DEFAULT_UPLOAD_ERROR = 'Something went wrong.';
    // Set to default values
    vm.notice = VIRGIN_NOTICE_2;
    vm.upload_error = '';

    // Set content_type value on click.
    vm.content_type_select = function(event) {
        vm.notice.content_type = parseInt(event.target.value);
    }

    // Check if data is filled correctly. Updates the error variable and return boolean.
    vm.is_valid_form = function() {
        // Content type should lie in range [0,3]
        if(vm.notice.content_type > 3 || vm.notice.content_type < 0) {
            vm.upload_error = 'Select Notice Content Type.';
            return false;
        }
        // Notice title length should be non-0
        if(vm.notice.title.length == 0){
            vm.upload_error = 'Notice Title Missing.';
            return false;
        }
        // More than one channel should be selected for publishing notice
        if(vm.notice.channel_ids.length == 0) {
            vm.upload_error = 'Channels to Post Notice Missing.';
            return false;
        }
        // Valid from date should be of correct format
        if(isNaN(vm.notice.valid_from.getTime())) {
            vm.upload_error = '\'Valid From\' date Incorrect.';
            return false;
        }
        // Valid till date should be of correct format
        if(isNaN(vm.notice.valid_till.getTime())) {
            vm.upload_error = '\'Valid Till\' date Incorrect.';
            return false;
        }
        // Valid from date should be selected
        if($('#valid-from-time').val()=="") {
            vm.upload_error = '\'Valid From\' time Incorrect.';
            return false;
        }
        // Valid from date should be selected
        if($('#valid-to-time').val()=="") {
            vm.upload_error = '\'Valid Till\' time Incorrect.';
            return false;
        }

        // vm.notice.valid_from = new Date($('#valid-from-date').val() + " " + $('#valid-from-time').val());
        // vm.notice.valid_till = new Date($('#valid-to-date').val() + " " + $('#valid-to-time').val());

        // Valid from data >= valid till date obviously
        if(vm.notice.valid_from >= vm.notice.valid_till) {
            vm.upload_error = '\'Valid From\' date-time should be less than \'Valid Till\' date-time.';
            return false;
        }

        // is file attached with the notice
        var is_file_attached = ($('#input-notice-file')[0].files.length>0);

        // notice content type specific details
        switch(vm.notice.content_type) {
            // Content type TEXT
            case 0:
                // desc should have more than 10 characters
                if(vm.notice.description.length < 10) {
                    vm.upload_error = 'Description should be atleast 10 characters long.';
                    return false;
                }
                vm.notice.link = '';
                break;
            // Content Type IMAGE
            case 1:
                // Either Attach File or Insert link
                if(vm.notice.link == '' && !is_file_attached) {
                    vm.upload_error = 'Either Attach File or Insert link.';
                    return false;
                }
                // Check for invalid links
                else if(!vm.notice.link.startsWith('http://') && !vm.notice.link.startsWith('https://') && !is_file_attached) {
                    vm.upload_error = 'Invalid Link URL.';
                    return false;
                }
                // if both are selected - file and link, use only file not link
                if(is_file_attached)
                    vm.notice.link = '';
                // Remove description
                vm.notice.description = '';
                break;
            // Content type PDF
            case 2:
                // Either Attach File or Insert link
                if(vm.notice.link == '' && !is_file_attached) {
                    vm.upload_error = 'Either Attach File or Insert link.';
                    return false;
                }
                // Check for invalid links
                else if(!vm.notice.link.startsWith('http://') && !vm.notice.link.startsWith('https://') && !is_file_attached) {
                    vm.upload_error = 'Invalid Link URL.';
                    return false;
                }
                // if both are selected - file and link, use only file not link
                if(is_file_attached)
                    vm.notice.link = '';
                // Remove description
                vm.notice.description = '';
                break;
            // Content type PPT
            case 3:
                // Only google sides link supported
                if(!vm.notice.link.startsWith('https://docs.google.com/presentation')) {
                    vm.upload_error = 'Invalid Link URL. Only google docs URLs are accepted for uploading PPT.';
                    return false;
                }
                // Remove description
                vm.notice.description = '';
                break;
        }
        return true;
    }

    // Add notice function
    vm.addNotice = function() {
        // Get the selected channels. List type
        vm.notice.channel_ids = [];
        for(var i=0; i<$scope.selectedChannels.length; i++)
            if($scope.selectedChannels[i].ticked)
                vm.notice.channel_ids.push($scope.selectedChannels[i].id);

        // Get validity from and till dates
        vm.notice.valid_from = new Date($('#valid-from-date').val() + " " + $('#valid-from-time').val());
        vm.notice.valid_till = new Date($('#valid-to-date').val() + " " + $('#valid-to-time').val());

        // change dates because of GMT-IST conversions. India Standard Time (IST) is 5:30 hours ahead of GMT
        vm.notice.valid_from.setHours(vm.notice.valid_from.getHours()+5);
        vm.notice.valid_from.setMinutes(vm.notice.valid_from.getMinutes()+30);
        vm.notice.valid_till.setHours(vm.notice.valid_till.getHours()+5);
        vm.notice.valid_till.setMinutes(vm.notice.valid_till.getMinutes()+30);

        // Convert to ISO string.
        console.log(vm.notice.valid_from.toISOString());

        // Check if the form is valid
        if(!vm.is_valid_form()) {
            // If not valid form, show the error response from the error variable
            console.log(vm.upload_error)
            $('#submit-notice-modal').modal({
                show: true,
            });
            return 'Found errors!!!';
        }

        // If valid add the form data
        var fd = new FormData();
        fd.append('data', angular.toJson(vm.notice));

        // Append file if selected
        if ((vm.notice.content_type == 1 || vm.notice.content_type == 2) && $('#input-notice-file')[0].files.length>0)
            fd.append('file', $('#input-notice-file')[0].files[0]);

        // Get the csrf cookie value
        var csrftoken = getCookie('csrftoken');

        // Post request using csrf token
        $http.post('/notices/submitNotice/', fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'X-CSRFToken': csrftoken
            }
        }).success(function (response) {
            // console.log(response);
            // If error show error returned by server
            if(response.status == 0) {
                vm.upload_error = response.info;
            }
            else {
                // Else no error. Show the notice with no error and success msg
                vm.upload_error = '';
                vm.notice = VIRGIN_NOTICE;
            }
            // Show the alert for the upload status
            $('#submit-notice-modal').modal({
                show: true,
            });
            return;
            // console.log(response);
        }).error(function (response) {
            // Show the default error
            vm.upload_error = DEFAULT_UPLOAD_ERROR;
            $('#submit-notice-modal').modal({
                show: true,
            });
            // console.log(response);
        });
    }

    // Not used. Get notices for a gicen channel
    getChannelNotices = function() {
        $http.get ('/notices/getChannelNotices/?channel_id=1').success(function (response) {
            if(response.error != 0) 
                return;
            // Update channel notices
            vm.channelNotices = response.channel_data;
            console.log(vm.channelNotices);
        }).error(function (response) {
            console.log(response);
        });
    }


}]);