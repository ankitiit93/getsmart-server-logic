var app = angular.module('smart-display-app', []);
var MAX_NOTIFICATIONS = 4;
var HOST = location.hostname+':'+location.port;
// Token in case of news being fetched ny client from zuppit server
var TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnX2lhdCI6MTQ1NDk2NDAzMCwidXNlcl9pZCI6ODc2LCJleHAiOjE0ODY1MDAwODQsInVzZXJuYW1lIjoicGlmaXNtYXJ0Ym9hcmRzIiwiZW1haWwiOiIifQ.8uowq4v0RYBcTLtKBO_2mIeQ60EqGoB8vYlpEzxoRnA';

// Main app controller
app.controller('MainController', ['$scope', '$http', '$interval', function($scope, $http, $interval) {
    var vm = this;
    // Notices variable
    vm.notices = [];
    // News List
    vm.newsList = [{
        title: 'Loading...',
        image: '/static/images/loading.gif'
    }];
    // Weather variable
    vm.weather = {};
    // clock card varibale
    vm.clock = Date.now();
    // Timetable if mrbs integrated
    vm.timeTable=[];
    // Current index of news displayed
    var newsIndexFocus = 0;
    // Current index of news displayed
    var noticeIndexFocus = 0;

    // Get latest notices from server 
    function getNotices() {
        $http({
            method: 'GET',
            url: 'http://' + HOST + '/notices/getNotices/',
            headers: {
                'Content-Type': "application/json"
            },
            params: {
                'id': ID
            }
        }).then(function successCallback(response) {
            console.log(response.data);
            // Check if no error from server
            if (response.data.data == null || response.data.data.length == 0 || response.data.status != 1)
                return;
            console.log('Notice Fetch - Success :)');
            // Change notices variable value with new data fetched
            vm.notices = response.data.data;
        }, function errorCallback(response) {
            console.log('Notices - Some Error Occurred :(');
        });

    }
    getNotices();
    // Update notices after every 30 minutes
    $interval(getNotices, 1800 * 1000); // 30 minutes

    // Get latest news info from server
    function updateNews() {
        $http({
            method: 'GET',
            url: 'http://' + HOST + '/notices/getNews/',
            headers: {
                'Content-Type': "application/json"
            },
        }).then(function successCallback(response) {
            // console.log(response);
            // Check if no error from server
            if(response.data.status != 1)
                return;
            // update news list variable value with new data fetched
            // console.log('News fetched successfullly');
            vm.newsList = response.data.data;
            // console.log(vm.newsList);
        }, function errorCallback(response) {
            console.log('News error detected');
            console.log(response);
        });

    }
    updateNews();
    // Update news after every 30 minutes
    $interval(updateNews, 1800 * 1000); // 30 minutes

    // Get latest weather info from server
    function updateWeather() {
        $http({
            method: 'GET',
            url: 'http://' + HOST + '/notices/getWeather/',
            headers: {
                'Content-Type': "application/json"
            },
        }).then(function successCallback(response) {
            // console.log(response);
            // Check if no error from server
            if(response.data.status != 1)
                return;
            // console.log('Weather fetched successfullly');
            //  Update weather data with latest one
            vm.weather = response.data.data;
        }, function errorCallback(response) {
            console.log('Weather error detected');
            // console.log(response);
        });

    }
    updateWeather();
    // update weather after every 30 minutes
    $interval(updateWeather, 1800 * 1000); // 30 minutes

    // Get new quote from server
    function updateQuote() {
        $http({
            method: 'GET',
            url: 'http://' + HOST + '/notices/getQuote/',
            headers: {
                'Content-Type': "application/json"
            },
        }).then(function successCallback(response) {
            // console.log(response);
            // Check if error on server
            if(response.data.status != 1)
                return;
            // console.log('Quote fetched successfullly');
            // Update the new quote
            vm.quote = response.data.data;
        }, function errorCallback(response) {
            console.log('Quote error detected');
            // console.log(response);
        });

    }
    updateQuote();
    // update the quotes after every 30 minutes
    $interval(updateQuote, 1800 * 1000); // 30 minutes

    // Update the clock timer
    function clockTick($scope, $timeout) {
        vm.clock = Date.now();
    }
    // Call the function after this interval again and again - 1 sec
    $interval(clockTick, 1*1000); // update clock in 1 sec

    // Control the news slider value
    function newsSlide() {
        // Scroll to top of next child in the news list
        $('.right-drawer').animate({
            scrollTop: $($(".right-drawer").children().get(newsIndexFocus)).offset().top - $($(".right-drawer").children().get(0)).offset().top
        }, 500);
        // Update the index in focus
        newsIndexFocus = (newsIndexFocus + 1) % (vm.newsList.length);
    }
    // Call the function after this interval again and again
    $interval(newsSlide, 6*1000); // 6 seconds
    
    // Control the notice slider values
    function noticeSlide() {
        // Scroll to top of next child in the notices list
        $('.main-content').animate({
            scrollTop: $($(".main-content").children().get(noticeIndexFocus)).offset().top - $($(".main-content").children().get(0)).offset().top
        }, 500);
        // Update the notice index in focus
        noticeIndexFocus = (noticeIndexFocus + 1) % (vm.notices.length);
    }
    // Call the function after this interval again and again
    $interval(noticeSlide, 10*1000); // 10 seconds

    // Check if reload required on the display
    function to_reload() {
        $http({
            method: 'GET',
            url: 'http://' + HOST + '/notices/reload/',
            headers: {
                'Content-Type': "application/json"
            },
            params: {
                'id': ID
            }
        }).then(function successCallback(response) {
            // console.log(response); 
            if(response.data.status != 1)
                return;
            // console.log('Reload info fetched successfullly');
            // If reload value is true then reload the page
            if(response.data.data.reload) {
                // alert('realoading page...');
                // reload
                location.reload();
            }
        }, function errorCallback(response) {
            console.log('Relaod check error detected');
            console.log(response);
        });
    }
    // Call the function after this interval again and again
    $interval(to_reload, 5*60*1000); // 5 minutes

}]);

// Create directives for all the cards structure found on the display
app.directive('piCard', function($interpolate) {
    return {
        scope: {
            // The data variable on the card
            data: '=info'
        },
        templateUrl: function(elem, attr) {
            // Template defining the DOM structure for the directive
            return 'http://'+ HOST +'/cards/'+attr.type+'_card/';
        }
    };
});