from django.db import models
from django.conf import settings
# settings.configure()
from django.contrib.auth.models import User
# from utilities import now_plus_days
from constants import CONTENT_CHOICES, TEXT
from datetime import datetime, timedelta
import os

""" Returns a timestamp object which is no_days after today """
def now_plus_days(no_days=3):
	return datetime.utcnow() + timedelta(days=no_days)


""" Channel basic details """
class Channel(models.Model):
    name = models.CharField(max_length=100)
    details = models.TextField(max_length=500)
    owner = models.CharField(max_length=100, default='owner')

    def __str__(self):
        return self.name


""" Display (TV) basic details """
class Display(models.Model):
    name = models.CharField(max_length=100)
    details = models.TextField(max_length=500)
    # geo codes for LCD location
    lat = models.FloatField(default=0)
    lon = models.FloatField(default=0)
    owner = models.CharField(max_length=100, default='owner')
    # List of channels subscribed by the display
    channel = models.ManyToManyField(Channel, related_name='display')
    # one time reload / reboot option
    to_reload = models.BooleanField(default=False)
    to_reboot = models.BooleanField(default=False)

    def __str__(self):
        return self.name        


""" Permissions for users - approve, admin """
class Permission(models.Model):
    APPROVER = 'AP'
    ADMIN = 'AD'
    UPLOADER = 'UP'
    PERMISSION_CHOICES = (
        (ADMIN, 'Can extend permissions'),
        (APPROVER, 'Can Approve content'),
        (UPLOADER, 'Can upload content'),
    )
    user = models.ForeignKey(User, db_index=True, related_name='user_with_perm')
    channel = models.ForeignKey(Channel, db_index=True, related_name='permissions')
    permissions = models.CharField(max_length=2, choices=PERMISSION_CHOICES, default=UPLOADER)

    def __str__(self):
        return str(self.user.username) + ' | ' + self.channel.name + ' | ' + self.permissions

    class Meta:
        unique_together = ('user', 'channel')


"""
    This is the global content upload system.
    One can upload infinite no of items here...

    Some Fields reference:
        - content_type: see constants.py for details
        - timestamp: The timestamp at which it was updated
        - In case of update, the is_approved flag is killed

"""
class Content(models.Model):

    content_type = models.CharField(max_length=5, choices=CONTENT_CHOICES, default=TEXT)
    title = models.CharField(max_length=50)
    # desc if notice type is text
    description = models.TextField(max_length=200, blank= True)
    # link to ppt/image/pdf in case a link is used
    link = models.CharField(max_length=500, blank=True)
    # file link in case file is uploaded
    user_file = models.FileField(upload_to='content/%Y/%m/', blank=True)
    # notice validity
    valid_from = models.DateTimeField('From Date', default=datetime.utcnow)
    valid_till = models.DateTimeField('Valid Till', default=now_plus_days)
    # notice upload time
    timestamp = models.DateTimeField(auto_now_add=True)
    submitter = models.ForeignKey(User, related_name='content_submitter')
    # channels on which notices can be posted
    channel = models.ManyToManyField(Channel)
    # approver for notice
    approver = models.ForeignKey(User, related_name='content_approver', null=True, blank=True)
    is_approved = models.BooleanField(default=False)
    approve_timestamp = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '%s, Type=%s' %(self.title, self.content_type)
    
    def get_file_link(self):
        return self.user_file.url
    # def value_to_string(self, obj):
    #     value = self.value_from_object(obj)
    #     return self.get_prep_value(value)


"""
    id : unique hexadecimal key for each quote
    author : name of the author, though data replication is problem here but
     it can be solved in future when pifi will become a big company
    text : text of the quote
    quotelink : permanent link of the quote which provides quote in html format
"""
class Quotes(models.Model):
    # Forismatic id
	id = models.CharField(max_length=10, primary_key=True)
    # Quote's author
	author = models.CharField(max_length=50)
    # quotes content
	text = models.CharField(max_length=1000)
    # Crawled url
	quotelink = models.CharField(max_length=300)


"""
    temperature : current temperature
    text : text of the weather condition
    quotelink : special code from list of codes yahoo weather api has, this code describes current weather condition and can be used to retrieve
    corresponding image icon
"""
class Weather(models.Model):

	temperature = models.IntegerField()
    # Weather condition in text
	text = models.CharField(max_length=100)
    # Weather condition Code to show weather icon
	code = models.IntegerField()
    # Last updated
	timestamp = models.DateTimeField(auto_now=True)


""" Zuppit News API data """
class News(models.Model):
    # Zuppit news id
	id = models.IntegerField(primary_key=True)
    # Zuppit news creation time
	zuppit_creation_timestamp = models.DateTimeField()
    # Zuppit news - source of news
	source_name = models.CharField(max_length=59)
    # news title
	title = models.CharField(max_length=300)
    # news URL
	original_url = models.CharField(max_length=300)
    # news image url
	image_link = models.CharField(max_length=300)
    # News summary for news
	preview = models.CharField(max_length=1000)
    # Last updated timestamp
	added_timestamp = models.DateTimeField(auto_now=True)
