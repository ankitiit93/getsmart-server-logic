
from datetime import datetime
import json, decimal
import requests as url_requests
from bs4 import BeautifulSoup
import json
from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.views.generic import View
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.utils.decorators import method_decorator
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core import serializers
from constants import *
from models import *
from utilities import *


# -------------- Views begin from here ----------------

"""
Gives 20 (defined by MAX_NOTICES) latest valid notices
"""
def get_notices(request):
    # get the display id of the display.
    display_id = getItem(request, 'id', default=1)
    if display_id is None:
        return errorReponse('Expected Display-ID along with request')
    # get current time
    curr_time = datetime.now()
    # Fetch notices valid during this time which are valid
    notices = Content.objects.select_related().filter( \
        channel__display__id=display_id, \
        is_approved=True, \
        valid_from__lte=curr_time, \
        valid_till__gte=curr_time \
        ).order_by('-timestamp').distinct()[0:MAX_NOTICES]
    published_content =[]
    # Transform them in required format
    for notice in notices:
        ans = {
            'id' : notice.id, \
            'type' : notice.content_type, \
            'title' : notice.title, \
            'description' : notice.description, \
        }
        if (notice.user_file):
            ans['url'] = SERVER + notice.user_file.url
        elif len(notice.link):
            ans['url'] = notice.link
        published_content.append(ans)
    # Send the reponse in json format
    return HRUtil(published_content)


"""
View function to handle submitted notice by any user
"""
def submit_notice(request):
    context = RequestContext(request)
    # Get the post request data while submitting notices
    data = json.loads(request.POST.get('data'))
    # Check if form is valid using a helper function which returns error if there is error
    is_valid_form = validate_submit_form(data, request.FILES);

    if is_valid_form != VALID_FORM:
        return errorReponse(is_valid_form)

    # Get rewuired fields from request
    content_type = data['content_type']
    title = data['title']
    description = data['description']
    link = data['link']
    channel_ids = data['channel_ids']
    valid_from = datetime.strptime(data['valid_from'][0:19], '%Y-%m-%dT%H:%M:%S')
    valid_till = datetime.strptime(data['valid_till'][0:19], '%Y-%m-%dT%H:%M:%S')
    files = request.FILES
    file = ''

    # if content type is image or pdf and link is none, then file should exist
    if((content_type == 1 or content_type == 2) and link == ''):
        try: 
            file = files['file']
        except:
            return errorReponse('No file or link found for Image or PDF')

    # if text type notice
    if (content_type == 0):
        link = '';
        content_type = TEXT

    # if image type notice
    elif (content_type == 1):
        if(len(files) != 0):
            link='';
        description = ''
        content_type = IMAGE

    # if pdf type notice
    elif (content_type == 2):
        if(len(files) != 0):
            link='';
        description = ''
        content_type = PDF

    # if ppt type notice
    elif (content_type == 3):
        description = ''
        content_type = PPT

    try:
        print channel_ids
        # create the model with the given fields
        uploaded_content = Content(content_type=content_type, title=title, description=description, link=link, user_file=file, valid_from=valid_from, valid_till=valid_till, submitter=request.user)
        # add model to db
        uploaded_content.save()
        # add all the channels to the notice on which it is posted
        uploaded_content.channel.add(*channel_ids)
        print 'reach'
    except Exception as e:
        print '%s (%s)' % (e.message, type(e))
        return errorReponse('Something went wrong')

    # Return response
    return HRUtil(data={}, info='Notice added successfully')


"""
Get news from local news database
"""
def get_news(request):
    context = RequestContext(request)
    # Get latest news model
    newsList = News.objects.values('title', 'image_link').order_by('-added_timestamp')[0:MAX_NEWS]
    # format data in required format
    data = [{
            'title': news['title'],
            'image': news['image_link'] 
        } for news in newsList
    ]
    return HRUtil(data)


"""
Api view to get current weather form local db
"""
def get_weather(request):
    context = RequestContext(request)
    # Get latest weather info
    weather = Weather.objects.values('temperature', 'text', 'code', 'timestamp').order_by('-timestamp').first()
    # format data in required format
    data =  {
        'text': weather['text'],
        'temperature': int(weather['temperature']),
        'icon': WEATHER_IMAGE[int(weather['code'])],
        'last_updated': str(weather['timestamp'])
    }
    return HRUtil(data)


"""
Api to get quote from quotation database
"""
def get_quote(request):
    context = RequestContext(request)
    # Get latest quotes info
    quote = Quotes.objects.values('author', 'text').order_by('?').first()
    # format data in required format
    data =  {
        'text': quote['text'],
        'author': quote['author'],
        'last_updated': str(datetime.now())
    }
    return HRUtil(data)


"""
Helps to reload the page on the client from the server
"""
def is_reload(request):
    # Get the display id
    pi_id = request.GET.get('id')
    try:
        pi_id = int(pi_id)
        # return the reload value. Set the value to false. So that it reloads only once.
        display = Display.objects.get(id = pi_id)
        reload_val = display.to_reload
        # Set to false and save the changes
        display.to_reload = False
        display.save()
    except:
        return errorReponse("Display ID does not exist")
    return HRUtil(data={'reload': reload_val})


"""
Register the cards as tags for client
"""
def pi_cards(request, card_type):
    context = RequestContext(request)
    # return the pi cards
    return render_to_response('cards/'+card_type+'.html', context)


"""
Approve content on Ajax Call
"""
def approve_content(request):
    # Check if user is authenticated
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    # Get the content (notice) id to be approved
    content_id = request.POST.get('content_id', '')
    #  Check if want to approve/unapprove content
    to_approve = int(request.POST.get('approve', -1))
    print to_approve
    # approval_status true - means that request was to approve content
    # approval_status false - means that request was to un-approve content
    approval_status = False
    if (to_approve == 1):
        approval_status = True

    if(content_id!=''):
        # Convert id to int
        content_id = int(content_id)
    try:
        # Get the content model to be approved
        content = Content.objects.get(id = content_id)
    except Content.ObjectDoesNotExist:
        return errorReponse('Supplied Content Id does not exist')
    print 'reach ' + str(approval_status)

    # Change to the new approval status
    content.is_approved  = approval_status
    content.approver  = request.user
    content.save()
    # Send required approval status
    if approval_status:
        return HRUtil('Content Approved Successfully')
    else:
        return HRUtil('Content Unapproved Successfully')


"""
Api required by AJAX call to add notice.
"""
def get_channels(request):
    channels = Channel.objects.all()
    published_content =[]
    # Get all available channels
    for channel in channels:
        ans = {
            'id' : channel.id, \
            'name' : channel.name, \
            'description' : channel.details, \
        }
        published_content.append(ans)
    return HRUtil(published_content)


"""
View function to handle edit notice queries by users
"""
def edit_notice(request):
    context = RequestContext(request)
    # Get the post request data while submitting notices
    data = json.loads(request.POST.get('data'))
    # Check if form is valid using a helper function which returns error if there is error
    is_valid_form = validate_submit_form(data, request.FILES);

    if is_valid_form != VALID_FORM:
        return errorReponse(is_valid_form)

    # Get rewuired fields from request
    content_id = data['id']
    content_type = data['content_type']
    title = data['title']
    description = data['description']
    link = data['link']
    valid_from = datetime.strptime(data['valid_from'][0:19], '%Y-%m-%dT%H:%M:%S')
    print valid_from.strftime('%Y-%m-%d %H:%M:%S')
    valid_till = datetime.strptime(data['valid_till'][0:19], '%Y-%m-%dT%H:%M:%S')
    files = request.FILES
    file = ''

    # if content type is image or pdf and link is none, then file should exist
    if((content_type == 1 or content_type == 2) and link == ''):
        try: 
            file = files['file']
        except:
            return errorReponse('No file or link found for Image or PDF')

    # if text type notice
    if (content_type == 0):
        link = '';
        content_type = TEXT

    # if image type notice
    elif (content_type == 1):
        if(len(files) != 0):
            link='';
        description = ''
        content_type = IMAGE

    # if pdf type notice
    elif (content_type == 2):
        if(len(files) != 0):
            link='';
        description = ''
        content_type = PDF

    # if ppt type notice
    elif (content_type == 3):
        description = ''
        content_type = PPT

    try:
        # Get the model to be edited
        edited_content = Content.objects.get(id=content_id);
        # Update all the fields with new ones
        edited_content.title=title
        edited_content.description=description
        edited_content.link=link
        edited_content.user_file=file
        edited_content.valid_from=valid_from
        edited_content.valid_till=valid_till
        # Save the model
        edited_content.save()
    except Exception as e:
        print '%s (%s)' % (e.message, type(e))
        return errorReponse('Something went wrong')

    # Return response
    return HRUtil(data={}, info='Notice edited successfully')


# ---------- SERVE HTML PAGES -----------


"""
Base site which can be extended to make any page on notice board admin site
"""
def base_site(request):
    # Base site template
    return render(request, 'admin_templates/base.html', {})


"""
Serve the client main page
"""
def index(request):
    context = RequestContext(request)
    # Get the main notice page
    context['id'] = getItem(request, 'id', default=1)
    return render_to_response('index.html', context)


"""
Serve the client main testing page - for dev purposes only
"""
def testing_template(request):
    context = RequestContext(request)
    # Template for testing before changing acutal URL
    context['id'] = getItem(request, 'id', default=1)
    return render_to_response('testing.html', context)


"""
Serve the login page if GET request.
Authenticate user. Go to home (/my_channels/)
If authenticated else redirect to login page
"""
def view_login(request, from_login=None):
    context = {}
    # If get return the template else authenticate
    if request.method == 'POST' and request.POST:
        logout(request)
        # Get username and password
        username = request.POST['username']
        password = request.POST['password']
        # Authenticate
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                # If authenticated then go to dashboard home page
                login(request, user)
                return HttpResponseRedirect('/my_channels/')
            else:
                # Else password is invalid
                context['errors'] = 'Invalid Password'
        else:
            context['errors'] = 'Invalid Password'
    return render(request, 'admin_templates/login.html', context)


"""
Serves the client logout page
"""
def view_logout(request):
    logout(request)
    # logout the user
    return render(request, 'admin_templates/logout.html', {})


"""
Serves the client add notices page
"""
def view_add_notice(request):
    # returnt the add notice page
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    channels = Channel.objects.all().values('id', 'name')
    # Populate context with the channels list for filling the dropdown list for multiselect channels
    context = RequestContext(request, {
        'channels': channels
    })
    print channels
    # Render the template with the context
    return render_to_response('admin_templates/add_notice.html', context)


"""
Get notice details for viewing
"""
def view_notice_details(request, content_id):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    # Get notice details if no editing is allowed
    content = Content.objects.get(id=int(content_id))
    return render(request, 'admin_templates/content_detail.html', locals())


"""
Edit notice details for viewing or editing
"""
def view_edit_notice(request, content_id):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')

    # Get notice details if editing is allowed.
    # If content id does not exist return error
    content = Content.objects.get(id=int(content_id))
    if (content.is_approved):
        return HttpResponseRedirect('/my_content/' + content_id + '/')
    else:
        return render(request, 'admin_templates/content_detail_edit.html', locals())


"""
View the notices posted by the user
"""
def view_my_contents(request):
    # Authenticate the user
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    context = {'error': 0}
    user = request.user
    # Get content submitted by the user
    my_content = Content.objects.filter(submitter = user).order_by('-timestamp')
    # channel_id = int(channel_id)
    # Format data in required format
    data = [
        {
            'id' : content.id, 
            'type' : content.content_type, 
            'title' : content.title, 
            'description':content.description, 
            'approved' : content.is_approved, 
            'link' : content.link, 
            'user_file' : content.user_file, 
            'timestamp' : content.timestamp, 
            'channel' : content.channel, 
            'approver':content.approver, 
            'approve_timestamp' : content.approve_timestamp, 
        }
        for content in my_content 
    ]
    context['my_content'] = data
    # Show contents using the data. content contains the data
    return render(request, 'admin_templates/my_contents.html', context)


"""
View the docs
"""
def view_docs(request):
    # Authenticate the user
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    context=[]
    # Show contents using the data. context contains the data
    return render(request, 'docs.html', context)


"""
Get channels where you have uploading permissions
"""
def view_my_channels(request):
    # Authenticate user
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    # Get the channel permissions with the user
    rows = Permission.objects.filter(
        user=request.user
    ).values('channel_id', 'channel__name', 'channel__details', 'permissions')
    admin_channels = []; approve_channels = []; upload_channels = []
    # Populate channels name with different permissions - admin, approve, upload
    for row in rows:
        if row['permissions'] == Permission.ADMIN:
            admin_channels.append(row)
        elif row['permissions'] == Permission.APPROVER:
            approve_channels.append(row)
        else:
            upload_channels.append(row)
    context = {}
    # Send context to DTL
    context['admin_channels'] = admin_channels
    context['approve_channels'] = approve_channels
    context['upload_channels'] = upload_channels
    return render(request, 'admin_templates/my_channels.html', context)


# ----------------- Unused functions -----------------
"""
    Get Channel Notices
"""
class get_channel_notices(APIView):

    def get(self, request, *args, **kwargs):
        params = request.query_params
        # Check if channel id exists in form data
        if 'channel_id' not in params:
            return errorReponse('Channel-ID Missing, we cannot supply notices if you donot give us channel_id')
        
        # Get channel id form request 
        channel_id = int(params['channel_id'])
        if(Channel.objects.filter(id=channel_id).exists()):
            channel = Channel.objects.get(id = channel_id)
        else:
            return Response({'msg':'Channel-ID Item Does exist in our database, please supply digestable channel_id', 'data' : [], 'error' :1})
        channel_published_content_data = Published_content.objects.filter(channel=channel)
        data = [{'id' : published_content_item.content.id, 'type' : published_content_item.content.content_type, 'title' : published_content_item.content.title, 'description':published_content_item.content.description, 'approved' : published_content_item.is_approved, 'publisher' : published_content_item.content.submitter.username} for published_content_item in channel_published_content_data]
        return Response({'msg':'You Retrieved Channel Content Successfully', 'channel_data' : data, 'error' :0})


def get_channel_notices_phone(request):
    id = getItem(request, 'id')
    if(Channel.objects.filter(id=id).exists()):
        channel = Channel.objects.get(id = id)
    else:
        return Response({'msg':'Channel-ID Item Does exist in our database, please supply digestable channel_id', 'data' : [], 'error' :1})
    curr_time = datetime.now()
    notices = Content.objects.filter( \
        channel=channel, \
        is_approved=True, \
        valid_from__lte=curr_time, \
        valid_till__gte=curr_time \
        ).order_by('-timestamp')[0:MAX_NOTICES]
    published_content =[]
    for notice in notices:
        ans = {
            'id' : notice.id, \
            'type' : notice.content_type, \
            'title' : notice.title, \
            'description' : notice.description, \
            'channel' : notice.channel.name, \
        }
        if (notice.user_file):
            ans['url'] = "http://notices.smartcampus.iitd.ernet.in" + notice.user_file.url
            # ans['url'] = "http://localhost"  notice.user_file.url
        elif len(notice.link):
            ans['url'] = notice.link
        published_content.append(ans)
    return HRUtil(published_content)


def view_channel_content_page(request, channel_id):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')

    try:
        channel_id = int(channel_id)
        channel = Channel.objects.get(id = channel_id)
    except:
        return errorReponse("Channel ID does not exist")

    # There shall be hopefully ONLY one db hit.
    admins = channel.permissions.filter(
        permissions=Permission.ADMIN
    )
    approvers = channel.permissions.filter(
        permissions=Permission.APPROVER
    )
    uploaders = channel.permissions.filter(
        permissions=Permission.UPLOADER
    )
    
    contents = Content.objects.filter(channel=channel).order_by('-timestamp')[
        0:MAX_CHANNEL_NOTICES_DISPLAY
    ]

    # Muaaah - the awesome locals() trick!!!
    return render(request, 'admin_templates/channel_content.html', locals())


"""
    To test connection to the server
"""
def is_up(request):
    return HRUtil("True")


"""
Converts the mrbs booking data to json string with schedule.
Gives the schedule for the asked timestamp, area (id e.g. 1) and room (id e.g. 1)
"""
def get_bookings_data(request):
    try:
        # Get request params required to hit query on mrbs website
        str_timestamp = getItem(request, 'timestamp')
        timestamp = datetime.strptime(str_timestamp, "%Y-%m-%d")
        area = int(getItem(request, 'area'))
        room = int(getItem(request, 'room'))
    except:
        return errorReponse('Bad input format')
    # get current day, month, year
    day = timestamp.day
    month = timestamp.month
    year = timestamp.year
    # complete the url for mrbs request
    base_url = 'https://poorvi.cse.iitd.ernet.in/mrbs/week.php?'
    whole_url = ''.join([base_url,'year=', str(year),'&month=', str(month),'&day=', str(day),'&area=', str(area),'&room=', str(room)])
    # print whole_url
    # verify=False - because of no certificate for Poorvi
    url_response = url_requests.get(whole_url, verify=False)
    # get html response
    html_response = url_response.text
    day_date = int(day);
    # parse html using beautifulsoup library
    soup = BeautifulSoup(html_response,'html.parser')
    # parse data in the json format required
    rows = soup.find_all('tbody')
    schedule_body = rows[3]
    all_rows = schedule_body.find_all('tr')
    i = 0;
    response = [];
    time_mapping = ['07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30']
    for row in all_rows:
        all_columns = row.find_all('td',{'class':"I"})
        for col in all_columns:
            current_day = col
            class_name = ''.join(current_day['class'])
            if(class_name=='I'):
                one_response = {}
                a_element = current_day.find('a')
                a_href = a_element['href']
                splited_href = str.split(a_href.encode('ascii','ignore'),'&')
                splited_day = str.split(splited_href[2],'=')
                if (int(splited_day[1])==day_date):
                    div_element = current_day.find('div')
                    # fill the reposne varibale
                    class_name = ''.join(div_element['class'])
                    title = a_element['title']
                    text = a_element.getText()
                    one_response['start_time'] = time_mapping[i]
                    one_response['title'] = title.encode('ascii','ignore')
                    one_response['text'] = text.encode('ascii','ignore')
                    class_time = int(class_name[7:])
                    one_response['duration'] = class_time*0.5

                    # else:
                    #   one_response['duration'] = 100;
                    response.append(one_response)
        i = i+1;
    return HRUtil(response)