# db_help.py - contains helper functions for accessing the database.
from django.conf import settings
from models import Quotes, Weather, News
from constants import *
from datetime import datetime, timedelta
import math
import json 
import requests
from django.http import HttpResponse as HR

# ------- views helper functions ---------

"""Converts a datetime object to UTC timestamp"""
def datetime_to_timestamp(dt):
    return int((dt.replace(tzinfo=None) - datetime(1970,1,1)).total_seconds())

""" get item from request key-value dictionary """
def getItem(request, key, default=None):
	# if request type GET
    if (USING_GET):
        return request.GET.get(key, default)
    return request.POST.get(key, default)

""" get item from request key-value dictionary """
def errorReponse(info):
	# Standard error response
    return HR(json.dumps({'status': 0, 'info': info, 'data': {}}), \
        content_type="application/json")

def HRUtil(data, info=''):
	# Standard HTTP response
    return HR(json.dumps({'status': 1, 'info': info, 'data': data}), \
        content_type="application/json")


# ------- database helper functions ---------

""" Format timestamp to string """
def format_TS(dt):
	return dt.strftime('%Y-%m-%d %H:%M:%S')

""" Because Timestamp is not json serializable, hence we change it in string format """
def __change_TS(dct):
	for k, v in dct.iteritems():
		if type(v) == datetime:
			dct[k] = format_TS(v)
	return dct

# ------- apis helper functions ---------

""" Get latest notices from database which are valid at the moment (0:MAX_NOTICES) """
def get_latest_notices():
	now = datetime.now()
	rows = Notice.objects.filter(from_timestamp__lt=now, to_timestamp__gt=now) \
		.order_by('-timestamp')[0:MAX_NOTICES]
	ans = [__change_TS(row) for row in rows.values()]
	for notice in ans:
		if ( bool(notice['image']) ):
			print str(notice)
			notice['image_link'] = settings.MEDIA_URL[1:] + notice['image']
		del notice['image']
	ans.reverse()
	return ans

""" Mines new quotes from source """
def quote_miner():
	# Add proxies if using IITD proxy
	response = requests.get(QUOTE_URL)#, proxies=PROXIES)
	if(response.status_code==200):
		response_data = response.json()
		quote_id = response_data['quoteLink'].split('/')[-2]
		new_quote = Quotes.objects.get_or_create(id=quote_id,\
			author = response_data['quoteAuthor'],\
			text = response_data['quoteText'],\
			quotelink = response_data['quoteLink'])


""" Mines updated weather information from source """
def weather_miner():
	# Add proxies if using IITD proxy
	response = requests.get(WEATHER_URL)#, proxies=PROXIES)
	if(response.status_code==200):
		# Parse response to db model
		response_data = response.json()
		condition_dict = response_data['query']['results']['channel']['item']['condition']
		temp = condition_dict['temp']
		condition = condition_dict['text']
		# add to db
		new_weather_item = Weather.objects.create(temperature=int(temp),\
			text = condition,\
			code = int(condition_dict['code']))

def news_miner():
	headers = {'Authorization': 'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnX2lhdCI6MTQ1NDk2NDAzMCwidXNlcl9pZCI6ODc2LCJleHAiOjE0ODY1MDAwODQsInVzZXJuYW1lIjoicGlmaXNtYXJ0Ym9hcmRzIiwiZW1haWwiOiIifQ.8uowq4v0RYBcTLtKBO_2mIeQ60EqGoB8vYlpEzxoRnA'}
	# Add proxies if using IITD proxy
	response = requests.get(NEWS_URL, headers=headers)#, proxies=PROXIES)
	if(response.status_code==200):
		# Parse response
		response_data = response.json()
		newsList = response_data['data']
		query_time = datetime.now()
		# Parse all news in the response
		for news in newsList:
			try:
				# add each news to db
				news_obj = News.objects.create(id = news['id'],\
					zuppit_creation_timestamp = datetime.fromtimestamp(float(news['creation_timestamp'])),\
					source_name = news['source_name'],\
					title = news['title'],\
					original_url = news['url'],\
					image_link = news['image_link'],\
					preview = news['preview'],\
					added_timestamp = query_time) 
			except:
				print 'Could not add news for some reason'

"""
Validate the notice form data and files for descrepencies in it.
Return 'valid' if form is valid othervise return the error resposne as string
"""
def validate_submit_form(data, files):
	try:
		# Get required fields from the data
		content_type = data['content_type']
		title = data['title']
		description = data['description']
		link = data['link']
		channel_ids = data['channel_ids']
		print files
	except:
		return 'Bad data received' 

	# Notice title should not be empty string
	if (len(title) == 0):
		return 'Title Missing'
	# Atleast one channel should exist to post notices
	if (len(channel_ids) == 0):
		return 'Channel To Post Missing'
	# Valid from and till data should be required format as expected
	try:
		valid_from = datetime.strptime(data['valid_from'][0:19], '%Y-%m-%dT%H:%M:%S')
	except:
		return 'Bad input for \'valid from\' date'
	try:
		valid_till = datetime.strptime(data['valid_till'][0:19], '%Y-%m-%dT%H:%M:%S')
	except:
		return 'Bad input for \'valid till\' date'

	# If notice content type is text then description id required
	if (content_type == 0):
		if (len(description) < 10):
			return 'Too Short Description'
	# If notice content type is image then either image link or file should exist
	if content_type == 1:
		if (link == '' and len(files)==0):
			return 'No file or link to get Image.';
			# Links should start with http ot https in case no file is added
		if (not (link.startswith('http://') or link.startswith('https://')) and len(files)==0):
			return 'Given link is not a valid URL';
	# If notice content type is pdf then either pdf link or file should exist
	if content_type == 2:
		if (link == '' and len(files)==0):
			return 'No file or link to get PDF.';
			# Links should start with http ot https in case no file is added
		if (not (link.startswith('http://') or link.startswith('https://')) and len(files)==0):
			return 'Given link is not a valid URL.';
	# Links should start with http ot https if content type id ppt
	if content_type == 3:
		if (not (link.startswith('http://') or link.startswith('https://'))):
			return 'Given link should begin with https://docs.google.com/presentation';

	# Return if form is valid othervise return the error resposne
	return VALID_FORM;


"""
Validate the edit notice form data
Return 'valid' if form is valid othervise return the error resposne as string
"""
def validate_edit_form(data, files):
	try:
		# Get required fields from the data
		content_type = data['content_type']
		title = data['title']
		description = data['description']
		link = data['link']
		print files
	except:
		return 'Bad data received' 

	# Notice title should not be empty string
	if (len(title) == 0):
		return 'Title Missing'
	# Valid from and till data should be required format as expected
	try:
		valid_from = datetime.strptime(data['valid_from'][0:19], '%Y-%m-%dT%H:%M:%S')
	except:
		return 'Bad input for \'valid from\' date'
	try:
		valid_till = datetime.strptime(data['valid_till'][0:19], '%Y-%m-%dT%H:%M:%S')
	except:
		return 'Bad input for \'valid till\' date'

	# If notice content type is text then description id required
	if (content_type == 0):
		if (len(description) < 10):
			return 'Too Short Description'
	# If notice content type is image then either image link or file should exist
	if content_type == 1:
		if (link == '' and len(files)==0):
			return 'No file or link to get Image.';
			# Links should start with http ot https in case no file is added
		if (not (link.startswith('http://') or link.startswith('https://')) and len(files)==0):
			return 'Given link is not a valid URL';
	# If notice content type is pdf then either pdf link or file should exist
	if content_type == 2:
		if (link == '' and len(files)==0):
			return 'No file or link to get PDF.';
			# Links should start with http ot https in case no file is added
		if (not (link.startswith('http://') or link.startswith('https://')) and len(files)==0):
			return 'Given link is not a valid URL.';
	# Links should start with http ot https if content type id ppt
	if content_type == 3:
		if (not (link.startswith('http://') or link.startswith('https://'))):
			return 'Given link should begin with https://docs.google.com/presentation';

	# Return if form is valid othervise return the error resposne
	return VALID_FORM;
