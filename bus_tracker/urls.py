from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views


app = 'notices'
urlpatterns = [
	url(r'^login/$', auth_views.login),
	url(r'^submitNotice/$', views.submit_notice),
	url(r'^editNotice/$', views.edit_notice),
	url(r'^getNotices/$', views.get_notices),
	url(r'^getNews/$', views.get_news),
	url(r'^getWeather/$', views.get_weather),
	url(r'^getQuote/$', views.get_quote),
	url(r'^reload/$', views.is_reload),
	url(r'^getChannels/$', views.get_channels),
	url(r'^getChannelNotices/$', views.get_channel_notices.as_view()),
	url(r'^getChannelNoticesPhone/$', views.get_channel_notices_phone),
]