from django.contrib import admin

# Register your models here.
from .models import Quotes, Weather, Display, Channel, \
	Permission, Content, News

class QuotesAdmin(admin.ModelAdmin):

	model = Quotes
	list_display = ['id', 'text', 'author', 'quotelink']


class WeatherAdmin(admin.ModelAdmin):

	model = Weather
	list_display = ['id', 'text', 'temperature', 'code']
		
# what to show on the admin panel
admin.site.register(Quotes, QuotesAdmin)
admin.site.register(Weather, WeatherAdmin)
admin.site.register(Display)
admin.site.register(Channel)
admin.site.register(Permission)
admin.site.register(Content)
admin.site.register(News)