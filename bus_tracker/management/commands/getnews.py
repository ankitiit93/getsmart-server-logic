# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from bus_tracker.utilities import news_miner

class Command(BaseCommand):
	def handle(self, *args, **options):
		news_miner()