# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from bus_tracker.utilities import weather_miner
from datetime import datetime

class Command(BaseCommand):


	def handle(self, *args, **options):
		dt = datetime.now()
		print('Get Weather started at : ' , dt)
		weather_miner()